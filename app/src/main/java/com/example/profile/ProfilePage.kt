package com.example.profile

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.activity_profile_page.*

class ProfilePage : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_profile_page)
        init()
    }
    private fun init(){
        Glide.with(this)
                .load("https://i1.wp.com/prosportsextra.com/wp-content/uploads/2020/11/IMG_6505.jpg?resize=700%2C500&ssl=1")
                .into(profile)

        Glide.with(this)
                .load("https://coverfiles.alphacoders.com/952/95236.jpg")
                .into(cover)
    }
}