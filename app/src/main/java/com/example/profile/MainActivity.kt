package com.example.profile

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        init()
    }
    private fun init(){

        signInButton.setOnClickListener{
            if (loginEmail.text.toString().isNotEmpty() && loginPassword.text.toString().isNotEmpty()){
                startActivity(Intent(this,ProfilePage::class.java))
                println("sign in")
            }
            else
            {
                Toast.makeText(this, "Please fill all field",Toast.LENGTH_SHORT).show()

            }

            }

        }
    }
